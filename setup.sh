#!/bin/bash

# Check if Docker is installed
if command -v docker &>/dev/null; then
    echo "Docker is already installed."
else
    # Install Docker
    echo "Docker is not installed. Installing Docker..."
    # Add Docker repository and install Docker
    sudo apt update
    sudo apt install -y docker.io
    # Start and enable Docker service
    sudo systemctl start docker
    sudo systemctl enable docker
    # Add your user to the docker group (optional, allows running Docker without sudo)
    sudo usermod -aG docker $USER
    echo "Docker is now installed and running."
fi
curl -o docker-compose.yaml -s https://gitlab.com/heizungssteuerenung/backend/-/raw/main/docker-compose.yml?ref_type=heads&inline=false
curl -o mosquitto.conf -s https://gitlab.com/heizungssteuerenung/backend/-/raw/main/mosquitto.conf?ref_type=heads&inline=false
sleep 1
docker compose up


