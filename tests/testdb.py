import sys
sys.path.append("..")

from src.db import MongoDBManager
import unittest
from datetime import datetime, timedelta
import mongomock

class TestMongoDBManager(unittest.TestCase):

    def setUp(self):
        # Initialisieren Sie den MongoDBManager vor jedem Test
        # Setzen Sie den MongoClient auf den simulierten mongomock Client
        mock_client = mongomock.MongoClient()
        self.db_manager = MongoDBManager(client=mock_client)

    def test_add_entry_temperature(self):
        temperature = 26.5
        date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # Updated format
        sensor_id = "sensor123"
        status_vent = "OPEN"
        status_pump = "ON"
        self.db_manager.add_entry_temperature(temperature, date, sensor_id, status_vent, status_pump)
        entries = self.db_manager.find_entries_temperature(date, date, sensor_id)
        self.assertEqual(len(entries), 1)
        self.assertEqual(entries[0]['Temperature'], temperature)
        self.assertEqual(entries[0]['status_vent'], status_vent)
        self.assertEqual(entries[0]['status_pump'], status_pump)

    def test_add_entry_ordertemperature(self):
        order_temp = "26"
        date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # Updated format
        sensor_id = "sensor123"
        self.db_manager.add_entry_ordertemperature(order_temp, date, sensor_id)
        entries = self.db_manager.find_entries_ordertemperature(date, date, sensor_id)
        self.assertEqual(len(entries), 1)
        self.assertEqual(entries[0]['OrderTemp in Celcius'], order_temp)

    def test_edit_entry_temperature(self):
        # Erstelle einen Eintrag
        temperature = 26.5
        date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # Updated format
        sensor_id = "sensor123"
        status_vent = "OPEN"
        status_pump = "ON"
        self.db_manager.add_entry_temperature(temperature, date, sensor_id, status_vent, status_pump)
        entries_before = self.db_manager.find_entries_temperature(date, date, sensor_id)
        
        # Ändere den Eintrag
        new_temp = 27.0
        new_status_vent = "CLOSED"
        new_status_pump = "OFF"
        self.db_manager.edit_entry_temperature(new_temp, date, sensor_id, new_status_vent, new_status_pump, entries_before[0]["id"])
        entries_after = self.db_manager.find_entries_temperature(date, date, sensor_id)

        # Überprüfe die Änderungen
        self.assertEqual(len(entries_after), 1)
        self.assertEqual(entries_after[0]['Temperature'], new_temp)
        self.assertEqual(entries_after[0]['status_vent'], new_status_vent)
        self.assertEqual(entries_after[0]['status_pump'], new_status_pump)

    def test_edit_entry_ordertemperature(self):
        order_temp = "26"
        date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # Updated format
        sensor_id = "sensor123"
        self.db_manager.add_entry_ordertemperature(order_temp, date, sensor_id)
        entries_before = self.db_manager.find_entries_ordertemperature(date, date, sensor_id)
        
        new_order_temp = "27"
        self.db_manager.edit_entry_ordertemperature(new_order_temp, date, sensor_id, entries_before[0]["Id"])
        entries_after = self.db_manager.find_entries_ordertemperature(date, date, sensor_id)

        self.assertEqual(len(entries_after), 1)
        self.assertEqual(entries_after[0]['OrderTemp in Celcius'], new_order_temp)

    def test_delete_old_data_temperature(self):
        old_date = (datetime.now() - timedelta(days=4*365)).strftime('%Y-%m-%d %H:%M:%S')  # Updated format
        sensor_id = "sensor123"
        status_vent = "OPEN"
        status_pump = "ON"
        self.db_manager.add_entry_temperature(26.5, old_date, sensor_id, status_vent, status_pump)
        self.db_manager.delete_old_data_temperature()
        entries = self.db_manager.find_entries_temperature(old_date, old_date, sensor_id)
        self.assertEqual(len(entries), 0)

    def test_delete_old_data_ordertemperature(self):
        old_date = (datetime.now() - timedelta(days=4*365)).strftime('%Y-%m-%d %H:%M:%S')  # Updated format
        sensor_id = "sensor123"
        self.db_manager.add_entry_ordertemperature("26", old_date, sensor_id)
        self.db_manager.delete_old_data_ordertemperature()
        entries = self.db_manager.find_entries_ordertemperature(old_date, old_date, sensor_id)
        self.assertEqual(len(entries), 0)

    def tearDown(self):
        # Aufräumen nach jedem Test (ist bei mongomock nicht unbedingt erforderlich, kann aber hilfreich sein)
        self.db_manager.db.temperature.delete_many({})
        self.db_manager.db.ordertemperature.delete_many({})

if __name__ == '__main__':
    unittest.main()
