# Import necessary modules
import sys
import unittest
from unittest.mock import MagicMock, patch

# Modify the system path to include the parent directory,
# allowing for imports from the src directory
sys.path.append("..")

# Import everything from schnitstellen module in src directory
from src.schnitstellen import *

# Import schnitstellen module for testing
import schnitstellen

# Define the test suite for schnitstellen module
class TestSchnitstellen(unittest.TestCase):

    # Mock the GPIO, time, and logging modules used in schnitstellen module
    # before each test
    @patch('schnitstellen.GPIO', autospec=True)
    @patch('schnitstellen.time', autospec=True)
    @patch('schnitstellen.logging', autospec=True)
    def setUp(self, mock_gpio, mock_time, mock_logging):
        # Store mock objects as instance variables for use in tests
        self.mock_gpio = mock_gpio
        self.mock_time = mock_time
        self.mock_logging = mock_logging

    # Test the get_temperature function by mocking the open function
    @patch('schnitstellen.open', new_callable=unittest.mock.mock_open, read_data="fake data")
    def test_get_temperature(self, mock_open):
        # Set up mock file data for the sensor file
        mock_open.return_value.__enter__.return_value.readlines.return_value = [
            '73 01 4b 46 7f ff 0c 10 1e : crc=1e YES\n',
            '73 01 4b 46 7f ff 0c 10 1e t=23125\n'
        ]
        sensor_id = "28-XXXXXXXXXXXX"
        # Call the function and check the result
        temperature = schnitstellen.get_temperature(sensor_id)
        self.assertEqual(temperature, 23.125)

    # Test the heizungssteuerung function with "OFF" argument
    def test_heizungssteuerung_off(self):
        schnitstellen.heizungssteuerung("OFF")
        # Check that the correct GPIO outputs were made
        self.mock_gpio.output.assert_any_call(schnitstellen.HEIZUNG_WASSERSPEICHER, False)
        self.mock_gpio.output.assert_any_call(schnitstellen.VENTIL_SCHLIESSEN, True)

    # Test the heizungssteuerung function with "ON" argument
    def test_heizungssteuerung_on(self):
        schnitstellen.heizungssteuerung("ON")
        # Check that the correct GPIO output was made
        self.mock_gpio.output.assert_any_call(schnitstellen.HEIZUNG_WASSERSPEICHER, True)

    # Test the check_vorlauf_temp function
    def test_check_vorlauf_temp(self):
        # Mock the get_temperature function to return a value higher than MAXTEMP_VORLAUF
        with patch('schnitstellen.get_temperature', return_value=schnitstellen.MAXTEMP_VORLAUF + 1):
            schnitstellen.check_vorlauf_temp()
        # Check that the correct GPIO output was made
        self.mock_gpio.output.assert_any_call(schnitstellen.VENTIL_SCHLIESSEN, True)

    # Test the set_ventil_position function
    def test_set_ventil_position(self):
        schnitstellen.set_ventil_position(50)
        # Check that the correct GPIO output was made
        self.mock_gpio.output.assert_any_call(schnitstellen.VENTIL_OEFFNEN, True)

    # Test the handle_hardware_error function
    def test_handle_hardware_error(self):
        schnitstellen.handle_hardware_error()
        # Check that the correct GPIO outputs were made
        self.mock_gpio.output.assert_any_call(schnitstellen.HEIZUNG_WASSERSPEICHER, False)
        self.mock_gpio.output.assert_any_call(schnitstellen.VENTIL_SCHLIESSEN, True)

    # Test the check_esp32_status function
    def test_check_esp32_status(self):
        # Set up a stale timestamp for the last ESP32 message
        schnitstellen.last_esp32_message = schnitstellen.time.time() - (schnitstellen.ESP32_TIMEOUT + 1)
        schnitstellen.check_esp32_status()
        # Check that the warning log was called with the correct message
        self.mock_logging.warning.assert_called_with("ESP32-Kommunikationsfehler!")

# Run the tests if the script is executed directly
if __name__ == '__main__':
    unittest.main()
