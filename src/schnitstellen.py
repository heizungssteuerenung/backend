# Check if running on Raspberry Pi
try:
    #Pip source Hinzufügen
    import RPi.GPIO as GPIO
except ImportError:
    # Mock RPi.GPIO for non-RPi platforms
    class MockGPIO:
        BOARD = None
        OUT = None
        @staticmethod
        def setmode(*args, **kwargs):
            pass
        @staticmethod
        def setwarnings(*args, **kwargs):
            pass
        @staticmethod
        def setup(*args, **kwargs):
            pass
        @staticmethod
        def output(*args, **kwargs):
            pass
        @staticmethod
        def cleanup():
            pass
    GPIO = MockGPIO

import time
import logging


# Logging-Konfiguration am Anfang der Datei
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(levelname)s - %(message)s',
                    handlers=[logging.FileHandler("schnittstellen.log", mode='a'),
                              logging.StreamHandler()])

# GPIO-Setup
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

# Digitale Ausgänge
HEIZUNG_WASSERSPEICHER = 8
VENTIL_OEFFNEN = 10
VENTIL_SCHLIESSEN = 12
UEBERSTEUERUNG_NACHT = 14

GPIO.setup(HEIZUNG_WASSERSPEICHER, GPIO.OUT)
GPIO.setup(VENTIL_OEFFNEN, GPIO.OUT)
GPIO.setup(VENTIL_SCHLIESSEN, GPIO.OUT)
GPIO.setup(UEBERSTEUERUNG_NACHT, GPIO.OUT)

# Werte einstellbar machen

# Defaultwerte
SOLLTEMP_WOHNRAUM = 22
SOLLTEMP_VORLAUF = 33
MAXTEMP_VORLAUF = 35
MISCHVENTIL_ZEIT = 90  # in Sekunden

# Zeitpunkt der letzten ESP32-Nachricht
last_esp32_message = time.time()
ESP32_TIMEOUT = 60  # 60 Sekunden ohne Nachricht von ESP32 gelten als Fehler

# DS18B20 Sensor IDs (replace with your actual sensor IDs)
SENSOR_ID_KF = "28-XXXXXXXXXXXX"  # Wasserspeicher
SENSOR_ID_WF = "28-XXXXXXXXXXXX"  # Wohnraum
SENSOR_ID_VF = "28-XXXXXXXXXXXX"  # Vorlauf
# Funktionen


def get_temperature(sensor_id):
    try:
        with open(f"/sys/bus/w1/devices/{sensor_id}/w1_slave", "r") as f:
            lines = f.readlines()

        if lines[0].strip()[-3:] != "YES":
            raise ValueError("Invalid data from DS18B20")

        temp_string = lines[1].split(" ")[-1].strip()
        temperature = float(temp_string[2:]) / 1000.0
        return temperature
    except FileNotFoundError:
        error_message = f"DS18B20 sensor {sensor_id} not found."
        handle_hardware_error(error_message)
    except ValueError as e:
        error_message = f"Error reading DS18B20: {e}"
        handle_hardware_error(error_message)

def heizungssteuerung(status):
    if status == "OFF":
        GPIO.output(HEIZUNG_WASSERSPEICHER, False)  # Turn off the heating storage
        GPIO.output(VENTIL_SCHLIESSEN, True)  # Close the valve
    else:
        GPIO.output(HEIZUNG_WASSERSPEICHER, True)  # Turn on the heating storage


def check_vorlauf_temp():
    temp_vf = get_temperature(SENSOR_ID_VF)
    if temp_vf > MAXTEMP_VORLAUF:
        GPIO.output(VENTIL_SCHLIESSEN, True)
        time.sleep(MISCHVENTIL_ZEIT)
        GPIO.output(VENTIL_SCHLIESSEN, False)


def set_ventil_position(percentage):
    # Check if percentage is within the valid range
    if not 0 <= percentage <= 100:
        logging.error("Invalid percentage value: %s. Must be between 0 and 100.", percentage)
        raise ValueError("Percentage must be between 0 and 100.")

    # Calculate the time to keep the valve open
    time_to_open = MISCHVENTIL_ZEIT * (percentage / 100.0)

    GPIO.output(VENTIL_OEFFNEN, True)
    time.sleep(time_to_open)
    GPIO.output(VENTIL_OEFFNEN, False)

def handle_hardware_error(error_message):
    print(f"Hardwarefehler erkannt: {error_message}")
    logging.error(f"Hardwarefehler erkannt: {error_message}")  # Log as error since it's an error condition
    GPIO.output(HEIZUNG_WASSERSPEICHER, False)


def check_esp32_status():
    global last_esp32_message
    if time.time() - last_esp32_message > ESP32_TIMEOUT:
        error_message = "ESP32-Kommunikationsfehler!"
        handle_hardware_error(error_message)
