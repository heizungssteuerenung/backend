import paho.mqtt.client as mqtt

class Mqtt_Receive:
    def __init__(self, topic):
        self.value = ""
        self.topic =topic
        client = mqtt.Client()
        client.on_connect = self.on_connect
        client.on_message = self.on_message

        client.connect("127.0.0.1", 1883, 60)
        client.loop_forever()

    def on_connect(self, client, userdata, flags, rc):
        client.subscribe(self.topic)

    def on_message(self, client, userdata, msg):
        self.value = str(msg.payload)
        

    def get_current(self):
        return self.value[2:-1]
