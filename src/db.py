from pymongo import MongoClient
from datetime import datetime
from datetime import timedelta
import logging
import mongomock


# Logging-Konfiguration am Anfang der Datei
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(levelname)s - %(message)s',
                    handlers=[logging.FileHandler("db.log", mode='a'),
                              logging.StreamHandler()])

class MongoDBManager:


    def __init__(self, client=None):
        if client:
            self.client = client
        else:
            self.client = MongoClient('mongo', 27017, username='root', password='sml12345')
        self.db = self.client['IPT81']

    """
    Zusammengefasst:
    Verwenden Sie localhost:27017 von Ihrem Host-System oder einem anderen externen System.
    Verwenden Sie mongo:27017 von einem anderen Container im selben Docker-Compose-Netzwerk.
    """

    def get_next_id(self, collection_name):
        counter = self.db.counters.find_one_and_update(
            {"_id": collection_name},
            {"$inc": {"seq": 1}},
            return_document=True
        )
        if counter:
            return counter["seq"]
        else:
            # Falls der Eintrag für die Sammlung nicht existiert, initialisieren und erneut abrufen
            self.db.counters.insert_one({"_id": collection_name, "seq": 1})
            return self.get_next_id(collection_name)

    # Zum Hinzufügen von Daten. 
    def add_entry_temperature(self, temperature, date, sensor_id, status_vent, status_pump):
        entry_id = self.get_next_id("temperature")
        temperature_data = {
            "Temperature": temperature,
            "status_vent": status_vent,
            "status_pump": status_pump,
            "id": str(entry_id),
            "Datum": date,
            "SensorId": sensor_id
        }
        self.db.temperature.insert_one(temperature_data)
        logging.info(f"Temperature entry added: {temperature_data}")

    def add_entry_ordertemperature(self, order_temp, date, sensor_id):
        entry_id = self.get_next_id("ordertemperature")
        order_temp_data = {
            "OrderTemp in Celcius": order_temp,
            "Id": str(entry_id),
            "Datum": date,
            "SensorId": sensor_id
        }
        self.db.ordertemperature.insert_one(order_temp_data)
        logging.info(f"Order temperature entry added: {order_temp_data}")

        #Automatische löschung von Daten
    def delete_old_data_temperature(self):
        # Daten löschen, die älter als drei Jahre sind
        three_years_ago = datetime.now() - timedelta(days=3*365)
        self.db.temperature.delete_many({"Datum": {"$lt": three_years_ago.strftime('%Y-%m-%d %H:%M:%S')}})  # Corrected to temperature collection
            
        # Mock the behavior for mongomock
        if isinstance(self.client, mongomock.MongoClient):
            stats = {"dataSize": 0}
        else:
            stats = self.db.command("dbstats")
            
        while stats["dataSize"] > 1.5e9:  # 3GB in Bytes
            # Löschen Sie die ältesten 1000 Datensätze (dies ist nur ein Beispielwert und kann angepasst werden)
            oldest_data = self.db.temperature.find().sort("Datum", 1).limit(1000)
            for record in oldest_data:
                self.db.temperature.delete_one({"_id": record["_id"]})
                
            # Update the stats variable, but only if not using mongomock
            if not isinstance(self.client, mongomock.MongoClient):
                stats = self.db.command("dbstats")

    def delete_old_data_ordertemperature(self):
        # Mock the behavior for mongomock
        if isinstance(self.client, mongomock.MongoClient):
            stats = {"dataSize": 0}
        else:
            stats = self.db.command("dbstats")

        # Daten löschen, die älter als drei Jahre sind
        three_years_ago = datetime.now() - timedelta(days=3*365)
        self.db.ordertemperature.delete_many({"Datum": {"$lt": three_years_ago.strftime('%Y-%m-%d %H:%M:%S')}})
            
        # Überprüfen Sie die Größe der Datenbank
        while stats["dataSize"] > 1.5e9:  # 3GB in Bytes
            # Löschen Sie die ältesten 1000 Datensätze (dies ist nur ein Beispielwert und kann angepasst werden)
            oldest_data = self.db.ordertemperature.find().sort("Datum", 1).limit(1000)
            for record in oldest_data:
                self.db.ordertemperature.delete_one({"_id": record["_id"]})

            # Update the stats variable, but only if not using mongomock
            if not isinstance(self.client, mongomock.MongoClient):
                stats = self.db.command("dbstats")


    # Abänderen von Daten
    def edit_entry_temperature(self, temperature, date, sensor_id, status_vent, status_pump, entry_id):
        self.db.temperature.update_one(
            {"id": entry_id},
            {"$set": {
                "Temperature": temperature,
                "Datum": date,
                "SensorId": sensor_id,
                "status_vent": status_vent,
                "status_pump": status_pump
            }}
        )

    def edit_entry_ordertemperature(self, order_temp, date, sensor_id, entry_id):
        self.db.ordertemperature.update_one(
            {"Id": entry_id},
            {"$set": {
                "OrderTemp in Celcius": order_temp,
                "Datum": date,
                "SensorId": sensor_id
            }}
        )

    # Finden von Daten
    def find_entries_temperature(self, start_date, end_date, sensor_id):
        return list(self.db.temperature.find({
            "Datum": {"$gte": start_date, "$lte": end_date},
            "SensorId": sensor_id
        }))

    def find_entries_ordertemperature(self, start_date, end_date, sensor_id):
        return list(self.db.ordertemperature.find({
            "Datum": {"$gte": start_date, "$lte": end_date},
            "SensorId": sensor_id
        }))

# Beispielverwendung:
# Klasse initialisieren
#db_manager = MongoDBManager()

# Datensatz hinzufügen:
#db_manager.add_entry_temperature(26.5, datetime.now().strftime('%d.%m.%Y: %H:%M:%S'), "sensor123", "100", "ON")
#db_manager.add_entry_ordertemperature("26", datetime.now().strftime('%d.%m.%Y: %H:%M:%S'), "sensor123")

# Daten aktualisieren
#db_manager.edit_entry_temperature(27.5, datetime.now().strftime('%d.%m.%Y: %H:%M:%S'), "sensor123", "CLOSED", "OFF", "2")
#db_manager.edit_entry_ordertemperature("27", datetime.now().strftime('%d.%m.%Y: %H:%M:%S'), "sensor123", "2")

# Datensatz finden:
#entries_temp = db_manager.find_entries_temperature("01.01.2021: 00:00:00", "31.12.2021: 23:59:59", "sensor123")
#entries_order_temp = db_manager.find_entries_ordertemperature("01.01.2021: 00:00:00", "31.12.2021: 23:59:59", "sensor123")

#print(entries_temp)
#print(entries_order_temp)  