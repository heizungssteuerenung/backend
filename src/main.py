from db import *
from schnitstellen import *
from datetime import datetime
from mqtt import *

# Initialize the database manager
db_manager = MongoDBManager()
mqtt_receive = Mqtt_Receive("temps/livingroom")

# Hauptprogramm
try:
    while True:
        # Fetch all ordered temperature entries for the living room from the database
        sensor_id_wf = SENSOR_ID_WF  # assuming this is the sensor id for the living room
        order_temp_entries = db_manager.find_entries_ordertemperature(None, None, sensor_id_wf)

        # Sort the entries by date, so the latest entry comes first
        order_temp_entries.sort(key=lambda x: x["Datum"], reverse=True)

        # Default temperature in case no matching ordered temperature entry is found
        SOLLTEMP_WOHNRAUM = 22  

        # Check each entry to see if the current time falls within the specified start and end date/times
        current_datetime = datetime.now()
        for entry in order_temp_entries:
            start_datetime = datetime.strptime(entry["Datum"], '%Y-%m-%d %H:%M:%S')
            end_datetime = entry.get("end_date")
            if end_datetime:
                end_datetime = datetime.strptime(end_datetime, '%Y-%m-%d %H:%M:%S')
            # If there's no end date, or the current time is within the range, update the target temperature
            if not end_datetime or start_datetime <= current_datetime <= end_datetime:
                SOLLTEMP_WOHNRAUM = float(entry["OrderTemp in Celcius"])
                break
        else:
            logging.warning("No matching ordered temperature entry found. Using default value.")

        # Heizung einschalten
        heizungssteuerung("ON")
        
        # ESP32 Status überprüfen
        check_esp32_status()

        # Temperaturen lesen
        temp_kf = get_temperature(SENSOR_ID_KF)
        temp_wf = float(mqtt_receive.get_current())
        temp_vf = get_temperature(SENSOR_ID_VF)
        
        print(f"Temp KF: {temp_kf}, Temp WF: {temp_wf}, Temp VF: {temp_vf}")
        logging.info(f"Temp KF: {temp_kf}, Temp WF: {temp_wf}, Temp VF: {temp_vf}")

        # Überprüfen, ob Vorlauftemperatur zu hoch ist
        check_vorlauf_temp()

        # Ventilposition basierend auf der Wohnraumtemperatur und Solltemperatur setzen
        if temp_wf < SOLLTEMP_WOHNRAUM:
            percentage_open = ((SOLLTEMP_WOHNRAUM - temp_wf) / SOLLTEMP_WOHNRAUM) * 100
            set_ventil_position(percentage_open)
        else:
            # Ventil schließen, wenn die Wohnraumtemperatur über der Solltemperatur liegt
            set_ventil_position(0)
        
        # Eine Sekunde warten, bevor der nächste Zyklus beginnt
        time.sleep(1)

except KeyboardInterrupt:
    # Bei einem KeyboardInterrupt (z.B. durch Drücken von Ctrl+C) die GPIOs säubern
    GPIO.cleanup()